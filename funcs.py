###############################################################################
# Author: Chandler Van Dyke
# Date: 2016/11/20
#
# Part 1: Python coding
# Provide the functions necessary to determine if a relation R over set A
# is reflexive, symmetric, transitive, and/or antisemmetric. You will only
# need to change code in this file, but can change the rels.py to add extra
# functionality if you desire. Your message returned for each property will
# list at least one necessary pair for the property to hold.
# For example, if relation ((1, 0), (0, 1)) over set (0, 1) was tested for
# the reflexive property, you could report that the (0, 0) is missing.
# (In the case of antisymmetry, you can list the offending pair as
# superfluous.)
#
# NOTE: To get credit for extra functionality in either file, you must
#       mention it in the README.txt file. If there is no mention of extra
#       functionality in the rels.py file, I reserve the option to run with
#       an original copy!
#
# Part 2: Data set
# You will provide a data set (via input file; change the file name below)
# that displays as many different combinations of relation properties as
# possible. For example, a set and relation that is not reflexive(r), not
# symmetric(s), not transitive(t), and not antisymmetric(a) would be one
# set of properties: rsta. A set and relation that is reflexive(R), is
# symmetric(S), not transitive(t), and not antisymmetric(a) would be: RSta.
# How many of the 16 possible combinations can you come up with?
# Include an example for each of the 16 possible combinations, and include
# a proof explanation in your README for each combination that is
# impossible.
#
# Your final submission will include:
# - this funcs.py file with your code in the appropriate functions
# - the rels.py file (with or without updates; you don't have to update.)
# - Your input data file (with filename in funcs.py below)
# - An output file from your run (change filename below as desired.)
# - Your README.txt file, including:
#   - Your write-up describing the 16 combinations
#   - Prooftext for each combination that you deem impossible
#   - List of bugs
#   - List of TODOs
#
###############################################################################

in_fname  = "input.txt"
out_fname = "output.txt"

def is_reflexive(A, R):
  # Determine if the relation R over set A is reflexive. The return value
  # will be a two-tuple of a boolean and a string message.

  # Create reflexive pairs of each element in A
  # Check if each pair is found in R
  # If not found, end loop and report element missing
  # Else say why R is reflexive
  if len(A)== 0:
      message = "reflexive because there are no elements in A, \n" \
                "   therefore there are no relations that are not\n" \
                "   reflexive"
  elif len(R)== 0 and len(A) == 0:
      message = "reflexive because there are no relations that are not \n" \
                "   reflexive, therefore the null set is reflexive."
  else:
      elementTuple = ()
      for element in A:
        elementTuple = (element, element)
        if elementTuple in R:
            message = "reflexive. For all x contained in A, (x, x)is \n" \
                      "   contained in  R."
        else:
            message = "not reflexive. Element {} was missing from R. " \
                      "Therefore\n" \
                      "   the related set is not " \
                      "reflexive.".format(elementTuple)
            break
  return True, message


def is_symmetric(A, R):
  # Determine if the relation R over set A is symmetric. The return value
  # will be a two-tuple of a boolean and a string message.

  # For each element in R, find the reciprocal
  # If no reciprocal is found, then explain that reciprocal is missing
  # Else explain that R is reciprocal.
  if len(A)== 0:
      message = "symmetric because there are no elements in A, \n" \
                "   therefore there are no relations that are not\n" \
                "   symmetric"
  elif len(R) == 0:
      message = "symmetric because there are no relations x to y, \n" \
                "   therefore the null set is symmetric."
  else:
      reverse = ()
      for pair in R:
          reverse = pair[::-1]
          if reverse in R:
              message = "symmetric. For all (x,y) contained in R, " \
                        "(y,x) is also\n"\
                        "   contained in R."
          else:
              message = "not symmetric. Element {} is missing, " \
                        "however {} was\n" \
                        "   present. Therefore R is not symmetric." \
                        "".format(reverse,pair)
              break

  return True, message


def is_transitive(A, R):
  # Determine if the relation R over set A is transitive. The return value
  # will be a two-tuple of a boolean and a string message.

  # For every x, y in Relation, if there is y, z, find x, z
  # if there is no x, z then the function is not transitive
  if len(A)== 0:
      message = "transitive because there are no elements in A, \n" \
                "   therefore there are no relations that are not\n" \
                "   transitive."
  elif len(R) < 2:
      message = "transitive because there are no relations x to y, \n" \
                "   and y to z such that x is related to z.\n" \
                "   Therefore the null set is transitive."
  else:
      message = "not transitive because for every x,y " \
                "there is no y,z contained\n"\
                "   in the relation."
      for pair in R:
          x = pair[0]
          y = pair[1]
          if x!= y:
              for pair in R:
                  if pair[0] == y:
                      z = pair[1]
                      if z != x and z != y:
                          if (x,z) in R:
                              message = "transitve becasue for every " \
                                        "(x,y) and\n"\
                                        "   (y,z), there exists " \
                                        "an (x,z) in\n" \
                                        "   the relation."
                          else:
                              message = "not transitive because " \
                                        "for one or more\n"\
                                        "   (x,y)and (y,z) " \
                                        "in the relation, an \n"\
                                        "   (x,z) does not exist. In this \n" \
                                        "   relation, {} is " \
                                        "missing.".format((x,z))

  # message = "transitive message"
  return True, message


def is_antisymmetric(A, R):
  # Determine if the relation R over set A is antisymmetric. The return value
  # will be a two-tuple of a boolean and a string message.

  # For each element (x,y) in R, find the reciprocal (y,x)
  # if y == x then R is antisymmetric.
  if len(A)== 0:
      message = "anti-symmetric because there are no elements in A, \n" \
                "   therefore there are no relations that are not\n" \
                "   anti-symmetric"
  elif len(R) == 0:
      message = "anti-symmetric because there are no relations x to y, \n" \
                "   therefore the null set is anti-symmetric."
  else:
      reverse = ()
      for pair in R:
          reverse = pair[::-1]
          if reverse in R and reverse[0]==reverse[1]:
              message = "anti-symmetric. " \
                        "For all (x,y) contained in R, (y,x) is\n"\
                        "   also contained in R where x = y."
          else:
              message = "not anti-symmetric. For all " \
                        "x,y in R, y,x does not \n"\
                        "   exist such that x = y."
              break

  return True, message


def is_irreflexive(A,R):
    # Determine if the relation R over set A is irreflexive. The return value
    # will be a two-tuple of a boolean and a string message.

    if len(A)== 0:
      message = "irreflexive because there are no elements in A, \n" \
                "   therefore there are no relations that are not\n" \
                "   irreflexive"
    elif len(R) == 0:
      message = "irreflexive because there are no relations that are not \n" \
                "   irreflexive, therefore the null set is irreflexive."
    else:
      elementTuple = ()
      for element in A:
        elementTuple = (element, element)
        if elementTuple in R:
            message = "not irreflexive. Element {} was contained from R. " \
                      "Therefore\n" \
                      "   the related set is not " \
                      "irreflexive.".format(elementTuple)
            break
        else:
            message = "irreflexive. For all x contained in A, (x, x)is \n" \
                      "   NOT contained in  R. For example if {} was in\n" \
                      "   R then the relation would not be irreflexive" \
                      ".".format(elementTuple)

    return True, message

