Set Stuff

Purpose:
The purpose of this project was to come up with an algorithm to sort through a given 
set of relations and determine if the relation was reflexive, symmetric, transitive,
or anti-symmetric.  Another part of the project was to come up with all the possible
outcomes of those defintions.  This turned out to be ten different outcomes.  Six are
impossible as described below.  


Extra Functionality:
The extra functionality in this project was the implimentation of irreflexive which I
programmed into the funcs.py file and adjusted the rels.py file in order to include
irreflexive in the results.


Definitions:

Reflexive: for all elements x in A, the element (x, x) is found in R.

Symmetric: for all elements x,y in A, if (x, y) is in R then (y, x) is in R

Transitive: for all x, y, z in A, if (x, y) and (y, z) is in R then (x, z) is in R

Anti-Symmetric: for all x, y in A, if (x, y) and (y, x) is in R, then x = y

(additional functionality) - irreflexive: for all x in A, element (x, x) is NOT in R.  



For the following:
R ==> element is Reflexive
r ==> element is not Reflexive
Y ==> element is Symmetric
y ==> element is not Symmetric
T ==> element is Transitive
t ==> element is not Transitive
A ==> element is Anti-Symmetric
a ==> element is not Anti-Symmetric

Rational for elements deemed impossible:

-All elements that are considered not symmetric but they are anti-symmetric are deemed not possible.
	This is impossible because if anti-symmetric is true then that means by definition that
	x is related to y and y is related to x which is also the definition of symmetric.  
	We can rewrite the definition of anti-symmetric through substitution and say:
	for all all x, y in A, if x and y are symmetric then x = y. 

	This means that the following elements are impossible:
	RyTA
	RytA
	ryTA
	rytA


-All elements that are considered not transitive and anti-symmetric are deemed not possible.
	This is impossible because if R is considered not transitive then elements (x, y) and (y, z)
	must exist and (x, z) must not exist in the set R or relation by definition of tranitive.
	Assuming both (x, y) and (y, z) exist and assuming x, y, z are all unique elements based on
	the definition of a set, then x is not equal to y and y does not equal z, and thus 
	anti-symmetric must be false.

	This means that the following elements are impossible (excluding those already deemed impossible):
	RYtA
	rYtA


-There are 16 possible combinations and 6 have been proved not possible above. The rest are possible
	and are listed below.  
-The following are all the possible outcomes as displayed in the running program.  See program for 
	proof texts.

	SET 1 = RYTA
	SET 2 = RYTa
`	SET 3 = RYta
	SET 4 = RyTa
	SET 5 = Ryta
	SET 6 = rYTA
	SET 7 = rYTa
	SET 8 = rYta
	SET 9 = ryTa
	SET 10 = ryta


Bugs:
-There are a couple loop holes in some of the definitions where no explination is printed
	for some given relation


To-Do:
-Make definitions more clear
-Definitions of Transitive, symmetric, and anti symmetric are unclear when relation or set is null.
	Clear up definition understanding to clear up difinitions in program.  