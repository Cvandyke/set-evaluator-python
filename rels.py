###############################################################################
# Author: Luke Kanuchok
# CST215 project #3 driver
# Define binary relation (from user? from file?) and determine the following
# Reflexivity, Symmetry, Transitivity, and Antisymmetry.
#
# Print out the results (is/isn't for each property), and if a property is
# missing, include one missing pair that would be required to be part of
# the relation.
#
# Also, print the matrix with labels showing relations
###############################################################################

from funcs import *
import re  # For Regexs used in user input

# Clear and open logging file
out_f = open(out_fname, 'w')


def log(text="", end="\n"):
  print(text, end=end)
  if out_f != None:
    out_f.write(text)
    if end!="":
      out_f.write(end)


def is_relation(A, R):
  # Validate the input data, a collection of values, and a collection of
  # pairs of values, the first element relating to the second element
  err_msg1 = " is not a list, set, or tuple"
  if not isinstance(A, (list, set, tuple)): return (False, "A"+err_msg1)
  if not isinstance(R, (list, set, tuple)): return (False, "R"+err_msg1)
  return (all(isinstance(r, tuple) and
             len(r)==2 and
             r[0] in A and
             r[1] in A
             for r in R),
          "R is not valid")


def print_matrix(A, R):
  log("  ", end="")  # space for vertical labels in first line
  log("".join(str(x) for x in A)) # Top labels

  for first_elem in A:
    # Print a row of the matrix
    log(str(first_elem)+" ", end="")  # vertical row label
    for second_elem in A:
      log("1" if (first_elem, second_elem) in R else "0", end="")
    log()


def test_relations(A, R):
  # Verify that set A and relation R are valid, then test and print results
  # for reflexivity, symmetry, transitivity, and anti-symmetry
  rel_test = is_relation(A, R)
  if (not rel_test[0]):
    log(str(A)+" and "+str(R)+" fail the relation test: "+rel_test[1])
    return

  log("A = "+str(A))
  log("R = "+str(R))
  print_matrix(A, R)

  for test in (is_reflexive, is_symmetric, is_transitive, is_antisymmetric,
               is_irreflexive):
    test_success, test_msg = test(A, R)
    log("R is "+test_msg)


def get_vals_from_string(line):
  # Extract the single letters and/or numbers from line, and return a tuple of
  # the found values
  set_re = re.compile("[a-zA-Z0-9]")
  to_val = lambda n: n if n not in '0123456789' else int(n)
  line = line[line.find('=')+1:].strip()
  ms = tuple(to_val(v) for v in set_re.findall(line))
  return ms


def get_pairs_from_string(line):
  # Extract the parenthesized pairs of values from line, and return a tuple of
  # pairs of found values
  relation_re = re.compile("([a-zA-Z0-9], [a-zA-Z0-9])")
  line = line[line.find('=')+1:].strip()
  ms = relation_re.findall(line)
  ms = tuple(tuple(get_vals_from_string(x)) for x in ms)
  return ms


def run_test_file(infname):
  """ Function reads a count from the beginning of the input file, and
  reads that many tests from the rest of the file (after an initial divider
  line):
    2
    ------------------------
    A=...
    R=...
    ------------------------
    A=...
    R=...
    ------------------------
  """
  with open(infname) as in_f:
    num_tests = int(in_f.readline())
    divider = in_f.readline() # Example: "---------------------------"
    log("Running {} tests...".format(num_tests))
    for nt in range(num_tests):
      Aline = in_f.readline().strip()
      Rline = in_f.readline().strip()
      divider = in_f.readline()

      log("Test #{}:".format(nt+1))
      log("Read Aline: \"{}\"".format(Aline))
      log("Read Rline: \"{}\"".format(Rline))

      if not Aline.startswith('A'):
        log("Missing A line: line starts with '{}'".format(Aline[0]))
        log(divider, end="")
        continue
      if not Rline.startswith('R'):
        log("Missing R line: line starts with '{}'".format(Rline[0]))
        log(divider, end="")
        continue

      # Keep the results as a tuple to force a recognizable order to values
      mySet = set(get_vals_from_string(Aline))
      myRelation = get_pairs_from_string(Rline)

      test_relations(mySet, myRelation)
      log(divider, end="")

run_test_file(in_fname)

out_f.close()
